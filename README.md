# shellcheck-codequality

Converts shellcheck json reports to GitLab Code Quality reports.

This is just a quick demo repo. Create the report with:

```bash
shopt -s globstar

shellcheck **/*.sh -f json1 > shellcheck.json
shellcheck-codequality
```
