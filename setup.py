from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="shellcheck-codequality",
    version="0.0.0",
    description="Converts shellcheck json reports to GitLab Code Quality reports",
    long_description=long_description,
    long_description_content_type="text/markdown",
    keywords="shellcheck gitlab code-quality codeclimate",
    url="https://gitlab.com/nejch1/shellcheck-code-quality.git",
    author="Nejc Habjan",
    author_email="hab.nejc@gmail.com",
    license="MIT",
    python_requires=">=3.6",
    py_modules=['shellcheck_codequality'],
    classifiers=[
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "License :: OSI Approved :: MIT License",
    ],
    entry_points={
        "console_scripts": [
            "shellcheck-codequality=shellcheck_codequality:main",
        ],
    },
)
