import argparse
import hashlib
import json
import sys
from collections import defaultdict


SEVERITY_MAPPING = {
    "error": "critical",
    "warning": "major",
    "info": "minor",
    "style": "info",
}

nested_defaultdict = lambda: defaultdict(nested_defaultdict)


def _get_fingerprint(filepath, rule, line):
    fingerprint = f"{filepath}:{rule}:{line}"
    return hashlib.md5(fingerprint.encode()).hexdigest()


def _create_issue(comment):
    issue = nested_defaultdict()

    issue["description"] = comment["message"]
    issue["severity"] = SEVERITY_MAPPING[comment["level"]]
    issue["location"]["path"] = comment["file"]
    issue["location"]["lines"]["begin"] = comment["line"]
    issue["fingerprint"] = _get_fingerprint(
        comment["file"], comment["code"], comment["line"]
    )

    return issue


def create_report(report):
    comments = report["comments"]
    return [_create_issue(comment) for comment in comments]


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input-file",
        default="shellcheck.json",
        help="Input file (shellcheck json report)",
    )
    parser.add_argument(
        "-o",
        "--output-file",
        default="shellcheck-codequality.json",
        help="Output file (GitLab Code Quality json report)",
    )
    args = parser.parse_args()

    try:
        with open(args.input_file, "rb") as f:
            data = json.load(f)
    except IOError as e:
        sys.exit("Failed to read from input file {}: {}".format(args.input_file, e))

    issues = create_report(data)

    try:
        with open(args.output_file, "w") as f:
            json.dump(issues, f, indent=4)
    except IOError as e:
        sys.exit("Failed to write to output file {}: {}".format(args.output_file, e))


if __name__ == "__main__":
    main()
